import sys

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from PyQt5.QtWebEngineCore import *
from PyQt5.QtWebEngineWidgets import *

import time

import html
import json

import requests
from genfunctions import *
import sqlfunctions

goal = ""
goal_title=""
tablename = ""

class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.resize(640,320)
        self.setWindowTitle("Wikipedia Jump")
        self.setWindowIcon(QIcon(QPixmap('favicon.ico')))

        self.pixmap = QPixmap('Wikipedia-Jump.png')
        self.image = QLabel()
        self.image.setPixmap(self.pixmap)    

        self.newgamebtn = QPushButton("New Game")
        self.highscoresbtn = QPushButton("High Scores")
        self.aboutgamebtn = QPushButton("About Game")
        self.exitgamebtn = QPushButton("Exit Game")

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.image)
        self.layout.addWidget(self.newgamebtn)
        self.layout.addWidget(self.highscoresbtn)
        self.layout.addWidget(self.aboutgamebtn)
        self.layout.addWidget(self.exitgamebtn)

        self.widget = QWidget()
        self.widget.setLayout(self.layout)
        self.setCentralWidget(self.widget)

        self.newgamebtn.clicked.connect(self.newgame)
        self.highscoresbtn.clicked.connect(self.highscores)
        self.aboutgamebtn.clicked.connect(self.aboutgame)
        self.exitgamebtn.clicked.connect(self.exitgame)


    def newgame(self):
        self.goalwindow = GoalWindow()
        self.goalwindow.show()
        self.hide()
    
    def highscores(self):
        self.highscores = highScoresGameView()
        self.highscores.show()
        self.hide()

    def aboutgame(self):
        self.aboutwindow = AboutWindow()
        self.aboutwindow.show()
        self.hide()

    def exitgame(self):
        sys.exit(app.exec())


class AboutWindow(QDialog):
    def __init__(self):
        super().__init__()

        self.resize(747,320)
        self.setWindowTitle("About Game - Wikipedia Jump")
        self.setWindowIcon(QIcon(QPixmap('favicon.ico')))

        self.pixmap = QPixmap('Wikipedia-Jump.png')
        self.image = QLabel()
        self.image.setPixmap(self.pixmap)

        self.label = QLabel("""The aim of this game is to start from a random Wikipedia article and reach another given article only using the links available in the article. It requires you to think about what articles could a link lead to, and which ones among those can take you to your final destination. Good luck, and happy reading!

Made by ~~~, ~~~ for CBSE board practicals 2022-23

Courtesy of Wikipedia, the free encyclopedia""")
        self.label.setWordWrap(True)

        self.backbtn = QPushButton("Back to main menu")

        layout = QVBoxLayout()
        layout.addWidget(self.image)
        layout.addWidget(self.label)
        layout.addWidget(self.backbtn)
        self.setLayout(layout)

        self.backbtn.clicked.connect(self.closeEvent)
        
    def closeEvent(self, event):
        self.mainwindow = MainWindow()
        self.mainwindow.show()
        self.hide()

class GoalWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.resize(747,320)
        self.setWindowTitle("The Goal - Wikipedia Jump")
        self.setWindowIcon(QIcon(QPixmap('favicon.ico')))

        self.pixmap = QPixmap('Wikipedia-Jump.png')
        self.image = QLabel()
        self.image.setPixmap(self.pixmap)  

        global goal
        goal = getLink()
        global goal_title
        goal_title = json.loads(requests.get(goal.replace('https://en.m.wikipedia.org/wiki/', 'https://en.wikipedia.org/api/rest_v1/page/summary/')).text)['title']
        goal_desc = json.loads(requests.get(goal.replace('https://en.m.wikipedia.org/wiki/', 'https://en.wikipedia.org/api/rest_v1/page/summary/')).text)['description']
        self.intro = QLabel(f"""The article you have to reach is <b>{html.escape(goal_title)}</b>.""")
        self.intro.setWordWrap(True)
        self.clue = QLabel(f"<b>Context clues:</b> {goal_desc}")
        self.startbtn = QPushButton("Start")
        self.startbtn.clicked.connect(self.start)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.image)
        self.layout.addWidget(self.intro)
        self.layout.addWidget(self.clue)
        self.layout.addWidget(self.startbtn)
        self.widget = QWidget()

        self.widget.setLayout(self.layout)
        self.setCentralWidget(self.widget)

    def start(self, event):
        global tablename
        tablename = generateNewTableName()
        sqlfunctions.addGame(tablename)
        self.gamewindow = gameWebView(self)
        self.gamewindow.show()
        self.hide()

class gameWebView(QDialog):
    def __init__(self, page):
        super().__init__(page)

        self.resize(747,320)
        self.setWindowTitle(f"{goal_title} - Wikipedia Jump")
        self.setWindowIcon(QIcon(QPixmap('favicon.ico')))
        self.webview = QWebEngineView()
        starturl= getLink()
        self.webview.load(QUrl(starturl))
        self.webview.urlChanged.connect(self.urlchange)

        self.toolboxwidget= QWidget()
        self.toolbox = QHBoxLayout()
        self.backbtn = QPushButton("Back")
        self.backbtn.clicked.connect(self.back)
        self.toolbox.addWidget(self.backbtn)
        self.toolbox.addStretch()
        self.toolboxwidget.setLayout(self.toolbox)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.toolboxwidget)
        self.layout.addWidget(self.webview)
        self.setLayout(self.layout)
        self.show()

    def closeEvent(self,event):
        sys.exit(app.exec())

    def back(self):
        hist = sqlfunctions.listClicks(tablename)
        html = generateHtml(hist[-2][0])
        self.webview.setHtml(html)
        sqlfunctions.addClick(tablename, hist[-2][0], time.time())

    def urlchange(self):
        if self.webview.url().url()[0:5] == "https":
            sqlfunctions.addClick(tablename, self.webview.url().url(), time.time())
        try:
            html = generateHtml(self.webview.url().url())
            self.webview.setHtml(html)
        except:
            pass
        if self.webview.url().url() == goal:
            self.endgame()
            
    def endgame(self):
        self.won = gameWonWindow()
        self.won.show()
        self.hide()

class gameWonWindow(QDialog):
    def __init__(self):
        super().__init__()

        self.resize(747,320)
        self.setWindowTitle("Congratulations - Wikipedia Jump")
        self.setWindowIcon(QIcon(QPixmap('favicon.ico')))
        self.layout = QVBoxLayout()
        self.pixmap = QPixmap('Wikipedia-Jump.png')
        self.image = QLabel()
        self.image.setPixmap(self.pixmap)
        self.text = QLabel(f"<h1>Congratulations on reaching the required page {goal_title}!</h1>")
        self.button = QPushButton("Return to main menu")
        self.button.clicked.connect(self.backtomenu)
        self.layout.addWidget(self.image)
        self.layout.addWidget(self.text)
        self.layout.addWidget(self.button)
        self.setLayout(self.layout)

    def backtomenu(self):
        self.menu = MainWindow()
        self.menu.show()
        self.hide()

class highScoresGameView(QDialog):
    def __init__(self):
        super().__init__()

        self.resize(747,320)
        self.setWindowTitle("High Scores - Wikipedia Jump")
        self.setWindowIcon(QIcon(QPixmap('favicon.ico')))
        self.layout = QVBoxLayout()
        self.pixmap = QPixmap('Wikipedia-Jump.png')
        self.image = QLabel()
        self.image.setPixmap(self.pixmap)

        self.gamesummary = QTableWidget(self)
        self.gamesummary.setColumnCount(4)
        data = sqlfunctions.listGames()
        for i in data:
            self.gamesummary.insertRow(0)

            date = i[0].lstrip('table')
            date = date[0:4]+'-'+date[5:7]+'-'+date[8:10]+' '+date[11:13]+':'+date[14:16]+':'+date[17:19]
            items=[date,html.escape(i[1].lstrip('https://en.m.wikipedia.org/wiki/').replace('_',' ')),html.escape(i[2].lstrip('https://en.m.wikipedia.org/wiki/').replace('_',' ')),str(round(i[3],2))]
            for i in range(4):
                self.gamesummary.setItem(0, i, QTableWidgetItem(items[i]))
        self.btn = QPushButton('See Details...')
        self.btn.clicked.connect(self.detailsview)
        self.gamesummary.resizeColumnsToContents()
        self.gamesummary.setSelectionMode(QAbstractItemView.SingleSelection)
        self.gamesummary.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.button = QPushButton("Return to main menu")
        self.button.clicked.connect(self.backtomenu)
        self.layout.addWidget(self.image)
        self.layout.addWidget(self.gamesummary)
        self.layout.addWidget(self.btn)
        self.layout.addWidget(self.button)
        self.setLayout(self.layout)

    def detailsview(self):
        row = self.gamesummary.selectedItems()[0].row()
        games = sqlfunctions.listGames()
        tablename = games[len(games)-row-1][0]
        clicks = sqlfunctions.listClicks(tablename)
        text = ''
        for i in clicks:
            title=html.escape(i[0].lstrip('https://en.m.wikipedia.org/wiki/').replace('_',' '))
            text+=title+'\t\t'+str(datetime.datetime.fromtimestamp(i[1]))[0:-4]+'\n'
        msgbox = QMessageBox(QMessageBox.Icon.Information,'Details - Wikipedia Jump', text)
        msgbox.setWindowIcon(QPixmap('favicon.ico'))
        msgbox.resize(747,320)
        msgbox.exec()

    def backtomenu(self):
        self.menu = MainWindow()
        
        self.menu.show()
        self.hide()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    mainwindow = MainWindow()
    mainwindow.show()
    sys.exit(app.exec())
