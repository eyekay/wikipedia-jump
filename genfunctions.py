# -*- coding: utf-8 -*-
"""
gamefunctions
-=-=-=-=-=-=-=

This module contains funtions for generating values for
various variables in the game.
"""
import requests
import random
import datetime

def generateHtml(currenturl):
    response = requests.get(currenturl)
    start = response.text.find('<main id="content"')
    end = response.text.find("</main>")
    # fix links
    html = response.text[start:end+7].replace('/wiki/', 'https://en.m.wikipedia.org/wiki/').replace('<a href="#', '<a href="'+currenturl+'#')
    return html

def getLink():
    i = random.randint(0,6)
    if i < 1:
        link = requests.get('https://en.m.wikipedia.org/wiki/Special:RandomInCategory/Featured_articles').url
    else:
        link = requests.get('https://en.m.wikipedia.org/wiki/Special:RandomInCategory/Good_articles').url
    return link

def generateNewTableName():
    tablename = 'table'+str(datetime.datetime.now()).replace(' ','_').replace(':','_').replace('-','_').partition('.')[0]
    return tablename
