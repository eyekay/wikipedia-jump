# -*- coding: utf-8 -*-
"""
sqlfunctions
-=-=-=-=-=-=-

This module contains Pythonic abstractions of SQL functions to manipulate the
database to store records of each game.

"""
import sqlite3 as sql

# Start a new game by creating a new table for it
def addGame(tablename):
    # Connect to database
    mycon = sql.connect("wikipedia_jump.db")
    cursor = mycon.cursor()
    cursor.execute(
        "create table {0}(link varchar(256),time float)".format(tablename)
    )
    mycon.commit()
    mycon.close()

# Add a new record for the opened article.
def addClick(name, newlink, time):
    mycon = sql.connect("wikipedia_jump.db")
    cursor = mycon.cursor()
    query = """INSERT INTO {table_name} VALUES({newlink},{time});""".format(
        time=time, table_name=name, newlink="'" + newlink + "'"
    )  # Reason for this hack: during query formatting the first text does not require being surrounded by apostrophes, but the second one does.
    cursor.execute(query)
    mycon.commit()
    mycon.close()

# Returns basic information about each game.
def listGames():
    mycon = sql.connect("wikipedia_jump.db")
    cursor = mycon.cursor()
    L = []
    # In MySQL this will be SHOW TABLES instead.
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
    tables = cursor.fetchall()
    for i in tables:
        name = i[0]
        cursor.execute("select * from {table_name}".format(table_name=i[0]))
        clicks = cursor.fetchall()
        print(clicks)        
        time = clicks[-1][1]-clicks[0][1]
        start_link = clicks[0][0]
        end_link = clicks[-1][0]
        L += [[name, start_link, end_link, time]]
    return L
    mycon.close()

# Returns detailed information about each game.
def listClicks(name):
    mycon = sql.connect("wikipedia_jump.db")
    cursor = mycon.cursor()
    cursor.execute("select * from {table_name}".format(table_name=name))
    clicks = cursor.fetchall()
    return clicks
    mycon.close()